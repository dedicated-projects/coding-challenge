<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InvoiceController;

Route::post('rate', [InvoiceController::class, 'calculate'])->name('invoice.calculate');

