<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Classes\Cdr;
use App\Classes\Rate;
use App\Services\InvoiceService;

class InvoiceServiceTest extends TestCase
{
    /**
     * @test
     */
    public function right_invoice_service_instance()
    {
        $invoiceService = new InvoiceService();
        $this->assertInstanceOf(InvoiceService::class, $invoiceService);
    }

    /**
     * @test
     */
    public function successful_calculating_invoice()
    {
        $invoiceService = new InvoiceService();
        $rate = new Rate([
            "energy" => 0.3,
            "time" => 2,
            "transaction" => 1
        ]);
        $cdr = new Cdr([
            "meterStart" => 1204307,
            "timestampStart" => "2021-04-05T10:04:00Z",
            "meterStop" => 1215230,
            "timestampStop" => "2021-04-05T11:27:00Z"
        ]);

        $response = $invoiceService->calculateInvoice($cdr, $rate);
        $this->assertNotEmpty($response);
        $this->assertEquals(7.04, $response);
    }
}
