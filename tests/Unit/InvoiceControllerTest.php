<?php

namespace Tests\Unit;

use Illuminate\Http\Response;
use Tests\TestCase;
use App\Classes\Cdr;
use App\Classes\Rate;
use App\Http\Requests\InvoiceRequest;
use Illuminate\Support\Facades\Validator;


class InvoiceControllerTest extends TestCase
{

    // RED Phase tests
    /**
     * @test
     */
    public function invalid_route_method()
    {
        $this->get(route('invoice.calculate'))
            ->assertStatus(Response::HTTP_METHOD_NOT_ALLOWED);
    }

    /**
     * @test
     */
    public function there_is_no_rate_object_in_request()
    {
        $cdr = [
            "meterStart" => 1257300,
            "timestampStart" => "2021-04-05T10:04:00Z",
            "meterStop" => 1258000,
            "timestampStop" => "2021-04-05T11:27:00Z",
        ];

        $response = $this->json("POST", route('invoice.calculate', ["cdr" => $cdr]));
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertClassHasAttribute('meterStart', Cdr::class);
    }

    /**
     * @test
     */
    public function there_is_no_cdr_object_in_request()
    {
        $rate = [
            "energy" => 0.3,
            "time" => 2,
            "transaction" => 1
        ];

        $response = $this->json("POST", route('invoice.calculate', ["rate" => $rate]));
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $response->getStatusCode());
        $this->assertClassHasAttribute('energy', Rate::class);
    }

    /**
     * @test
     */
    public function validation_error_occurred()
    {
        $validator = Validator::make(
            [
                'rate' => "",
                'cdr' => ""
            ],
            (new InvoiceRequest())->rules());

        $this->assertFalse($validator->passes());
    }


    //GREEN Phase tests

    /**
     * @test
     */
    public function successful_redirection_to_invoice_route()
    {
        $this->post(route('invoice.calculate'))
            ->assertStatus(Response::HTTP_FOUND);
    }

    /**
     * @test
     */
    public function successful_request_validation()
    {
        $input = [
            'rate' => ["energy" => 0.3, "time" => 2, "transaction" => 1],
            'cdr' => [
                "meterStart" => 1204307,
                "timestampStart" => "2021-04-05T10:04:00Z",
                "meterStop" => 1215230,
                "timestampStop" => "2021-04-05T11:27:00Z"
            ],
        ];

        $this->post(route('invoice.calculate'), $input)
            ->assertStatus(Response::HTTP_CREATED);
        $this->assertClassHasAttribute('energy', Rate::class);
        $this->assertClassHasAttribute('meterStart', Cdr::class);
    }

    /**
     * @test
     */
    public function return_valid_response()
    {
        $input = [
            'rate' => ["energy" => 0.3, "time" => 2, "transaction" => 1],
            'cdr' => [
                "meterStart" => 1204307,
                "timestampStart" => "2021-04-05T10:04:00Z",
                "meterStop" => 1215230,
                "timestampStop" => "2021-04-05T11:27:00Z"
            ],
        ];

        $this->json('post', route('invoice.calculate'), $input)
        ->assertStatus(Response::HTTP_CREATED)
        ->assertJsonStructure(
            [
                "overall",
                "components" => [
                    'energy',
                    'time',
                    'transaction',
                ]
            ]
        );
    }
}
