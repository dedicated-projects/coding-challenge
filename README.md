### Coding Challenge Document

You can see complete document below:
***
**Sections**
1. InvoiceController

In this class, you can see a construction method that has service injection for calculating invoices. First of all, in the calculate method, input data will be passed, then a class named InvoiceRequest will validate that. After validation, InvoiceService will call and calculate the invoice of the charging process. Finally, in output, you will see an object which will contain an overall price & rate values.

2. InvoiceService

In this class, after receiving input, the timestamp values will convert from ISO 8601 to UTC, then the difference between them will calculate. Finally, the overall value of the invoice will return.

3. InvoiceRequest

In this class, all fields of the input of InvoiceController will validate. If occurred any validation error, it will be shown the proper message, and you can see it in the output. 

4. Unit Tests

In this directory, there are two classes: 

* InvoiceServiceTest
In this class, there are two tests for checking InvoiceService class.

* InvoiceControllerTest
In this class, there are seven tests for checking InvoiceController class.

5. Route

In this directory, there is a class named api.php for determining the API route with address '/rate' with the POST method.
***
**How to run this project**

1. Open cmd and enter this command: **git clone https://gitlab.com/dedicated-projects/coding-challenge.git**
2. Then, enter this command: composer install (for installing all dependencies)
3. After that, enter this command: php artisan serve (for starting Laravel development server: http://127.0.0.1:8000) 
4. Open Postman and enter the route: http://localhost:8000/api/rate
5. Post inputs as below:

**Input**

{
"rate": { "energy": 0.3, "time": 2, "transaction": 1 },
"cdr": { "meterStart": 1204307, "timestampStart": "2021-04-05T10:04:00Z", "meterStop": 1215230, "timestampStop":
"2021-04-05T11:27:00Z" }
}

6. Finally, you can see output as below:

**Output**

{
"overall": 7.04
"components": { "energy": 3.277, "time": 2.767, "transaction": 1 }
} 
***
**Some Tips**

1. There is a GitLab address for checking related repository as below:

2. Gitlab repository address (https://gitlab.com/dedicated-projects/coding-challenge.git)

3. Because there are no other tasks, so the GitLab repository has no another branch and the only branch is master.
***
**How to run tests**

For running all tests, just enter the following command in cmd:

**vendor\bin\phpunit**
***
**Challenge 2 - API Design Improvements**

I think there are two attempts to improve the API design based on the best practice.

**Attempt 1:**

In this solution, we should create a model (for Rate Entity) and put all the related getters in it. Then, we will have an endpoint (named rates) for working with rate CRUD (such as post, get, put to insert, select, and update the rate record, respectively) as below:

* POST     /rates (for inserting new record in rates table)
* GET      /rates (for selecting a rate collection from rates table)
* PUT     /rates/id (for updating a specific rate record in rates table)

**Note:**

As we all know, we must use plural words instead of single ones for naming the endpoints (in REST API), so we will use /rates instead of /rate.

We will have a challenge here. If we use the “GET /rates” endpoint, we will fetch all the rate records and then, we must select the intended rate id. Finally, we must pass two arguments to the desired method for calculating the charging process invoice (for example calculateCdrPrices (cdr, rate_id)).

Related endpoint: POST  /commands/calculate_invoice

**Attempt 2:**

In this solution, in addition to saving the rate records in the related table, we can have a table (named cdrs) and a model named Cdr (for Cdr Entity). Then, we can have some other endpoints as below:

* POST     /cdrs (for inserting new record in cdrs table)
* GET      /cdrs (for selecting cdr collection from drs table)
* PUT     /cdrs/id (for updating a specific cdr record in cdrs table)

After that, we will have two IDs (for rate and cdr) and it is enough to pass them to the desired method to calculate the invoice (for example getCalculations(cdr_id, rate_id)).

**Final Tips:**

1. We can use CRUD queries in a database (MySQL, Redis, etc.) or use Laravel Cache for more use in the future.
2. If we want to have only a unique rate value (and no need to have different rates), we can generate a seeder class for the rates table, and when we migrate the database tables, we can seed the rates table. So, we will have the intended record, and we have no challenge in selecting the desired rate_id.
