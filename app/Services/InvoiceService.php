<?php

namespace App\Services;

use App\Classes\Cdr;
use App\Classes\Rate;
use Carbon\Carbon;

class InvoiceService
{
    /*
     * After receiving input (including cdr & rate) in this method
     * it will return the amount of overall value of this invoice together with the rate object input
     */
    public function calculateInvoice(Cdr $cdr, Rate $rate) :float
    {
        //Convert ISO 8601 to UTC timezone (string to dateTime) & calculate the difference between them
        $timestampStart = Carbon::parse($cdr->getTimestampStart());
        $timestampStop = Carbon::parse($cdr->getTimestampStop());
        $timestampValue = ($timestampStart)->diffInMinutes($timestampStop);

        //Calculate the difference between start & start meter
        $meterValue = (($cdr->getMeterStop()) - ($cdr->getMeterStart())) / 1000;

        //Get the rate values
        $energy = $rate->getEnergy();
        $valuePerMinute = ($rate->getTime()) / 60;
        $transaction = $rate->getTransaction();

        //Calculate & return the overall value
        $overall = ($energy*$meterValue) + ($valuePerMinute*$timestampValue) + ($transaction);
        return  round($overall, 2);
    }
}
