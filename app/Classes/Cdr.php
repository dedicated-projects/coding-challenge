<?php

namespace App\Classes;

class Cdr
{
    private $meterStart;
    private $timestampStart;
    private $meterStop;
    private $timestampStop;

    public function __construct($cdr)
    {
        $this->meterStart = $cdr['meterStart'];
        $this->timestampStart = $cdr['timestampStart'];
        $this->meterStop = $cdr['meterStop'];
        $this->timestampStop = $cdr['timestampStop'];
    }

    public function getMeterStart()
    {
        return $this->meterStart;
    }

    public function getTimestampStart()
    {
        return $this->timestampStart;
    }

    public function getMeterStop()
    {
        return $this->meterStop;
    }

    public function getTimestampStop()
    {
        return $this->timestampStop;
    }
}
