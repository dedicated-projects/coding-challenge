<?php

namespace App\Classes;

class Rate
{
    private $energy;
    private $time;
    private $transaction;

    public function __construct($rate)
    {
        $this->energy = $rate['energy'];
        $this->time = $rate['time'];
        $this->transaction = $rate['transaction'];
    }

    public function getEnergy()
    {
        return $this->energy;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getTransaction()
    {
        return $this->transaction;
    }
}
