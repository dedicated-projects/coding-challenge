<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'cdr.meterStart' => 'required|integer',
            'cdr.timestampStart' => 'required',
            'cdr.meterStop' => 'required|integer',
            'cdr.timestampStop' => 'required',
            'rate.energy' => 'required',
            'rate.time' => 'required',
            'rate.transaction' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'cdr.meterStart.required' => 'Start meter value is a required field.',
            'cdr.meterStart.integer' => 'Start meter value should be integer',
            'cdr.timestampStart.required' => 'Start timestamp is a required field.',
            'cdr.meterStop.required' => 'Stop meter is a required value.',
            'cdr.meterStop.integer' => 'Stop meter value should be integer',
            'cdr.timestampStop.required' => 'Stop timestamp is a required field.',
            'rate.energy.required' => 'Energy fee is a required field.',
            'rate.time.required' => 'Time fee is a required field.',
            'rate.transaction.required' => 'Transaction fee is a required field.',
        ];
    }
}
