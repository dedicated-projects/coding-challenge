<?php

namespace App\Http\Controllers;

use App\Classes\Cdr;
use App\Classes\Rate;
use Illuminate\Http\Response;
use App\Services\InvoiceService;
use App\Http\Requests\InvoiceRequest;
use App\Exceptions\CalculateInvoiceException;

class InvoiceController extends Controller
{
    public InvoiceService $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        $this->invoiceService = $invoiceService;
    }

    /*
     * After receiving input (including cdr & rate), InvoiceService will call
     * Finally, it will return the structure of the output (including overall value and rate values)
     */
    public function calculate(InvoiceRequest $request)
    {
        $validated = $request->validated();

        if (isset($validated->message)) {
            return response()->json($validated, Response::HTTP_UNPROCESSABLE_ENTITY);

        }
        try {
            $cdr = $request->cdr;
            $rate = $request->rate;

            $cdr = new Cdr($cdr);
            $rate = new Rate($rate);

            $overall = $this->invoiceService->calculateInvoice($cdr, $rate);

            $calculatedInvoice = [
                "overall" => $overall,
                "components" => $request->rate
            ];

            return response()->json($calculatedInvoice, Response::HTTP_CREATED);
        } catch (CalculateInvoiceException $e) {
            throw new CalculateInvoiceException('An error occurred while calculating the invoice.');
        }
    }
}
